package com.indorse.friend.book.user.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.indorse.friend.book.entities.UserProfile;
import com.indorse.friend.book.repositories.UserProfileRepository;
import com.querydsl.core.types.Predicate;

import static com.indorse.friend.book.repositories.UserPredicates.withOtherUser;

@Service
public class UserListService {

	private final UserProfileRepository userProfileRepository;
	
	public UserListService(UserProfileRepository userProfileRepository) {
		this.userProfileRepository = userProfileRepository;
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public Page<UserProfile> execute(UserListRequest request) throws Exception {
		Predicate userPredicate = withOtherUser(request.getUserId());
		Page<UserProfile> users = userProfileRepository.findAll(userPredicate, request.getPageable());
		return users;
	}
}
