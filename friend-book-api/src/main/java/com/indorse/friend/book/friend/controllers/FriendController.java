package com.indorse.friend.book.friend.controllers;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indorse.friend.book.commons.RestAPIException;
import com.indorse.friend.book.friend.service.FriendRequest;
import com.indorse.friend.book.friend.service.FriendService;

@RestController
public class FriendController {
	private final FriendService friendService;
	
	public FriendController(FriendService friendService) {
		this.friendService = friendService;
	}
	
	@RequestMapping(
            value = "/user/add/friend",
            method = RequestMethod.POST)
    public Boolean addFriend(@RequestBody FriendRequest request) throws Exception {
		if(validateRequest(request)) {
	    	return friendService.execute(request);
		} 
		return null;
    }
	
	private boolean validateRequest(FriendRequest request) throws Exception {
		if(request.getUserId() == null) {
			throw new RestAPIException("Bad request, user id is null");
		} else if(request.getFriendId() == null) {
			throw new RestAPIException("Bad request, friend id is null");
		}
		return true;
	}
}
