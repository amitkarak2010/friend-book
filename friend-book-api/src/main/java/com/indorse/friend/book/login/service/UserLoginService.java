package com.indorse.friend.book.login.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.indorse.friend.book.commons.NotFoundException;
import com.indorse.friend.book.commons.RestAPIException;
import com.indorse.friend.book.commons.jwt.GenerateJWT;
import com.indorse.friend.book.entities.UserProfile;
import com.indorse.friend.book.mappings.UserMappingService;
import com.indorse.friend.book.models.UserProfileDTO;
import com.indorse.friend.book.repositories.UserProfileRepository;
import com.indorse.friend.book.utils.PasswordService;
import com.querydsl.core.types.Predicate;

import static com.indorse.friend.book.repositories.UserPredicates.withEmail;
@Service
public class UserLoginService {

	private final UserProfileRepository userProfileRepository;
	private final UserMappingService userMappingService;
	private final PasswordService passwordService;
	
	public UserLoginService(UserProfileRepository userProfileRepository,
			UserMappingService userMappingService,
			PasswordService passwordService) {
		this.userMappingService = userMappingService;
		this.userProfileRepository = userProfileRepository;
		this.passwordService = passwordService;
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public UserProfileDTO execute(UserLoginRequest request) throws Exception {
		Predicate emailPredicate = withEmail(request.getEmail());
		UserProfile user = userProfileRepository.findOne(emailPredicate);
		if(user == null) {
			throw new NotFoundException("User with email " + request.getEmail() + " not found");
		}
		
		boolean isValidUser = passwordService.matchPassword(request.getPassword(), user.getPassword());
		if(isValidUser) {
			String token = GenerateJWT.generateToken(user.getFirstName(), new String[] {"API"});
			UserProfileDTO userProfile = userMappingService.map(user);
			userProfile.setApiToken(token);
			return userProfile;
		} else {
			throw new RestAPIException("Password is invalid");
		}
	}
}
