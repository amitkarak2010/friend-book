package com.indorse.friend.book.config;

import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.indorse.friend.book.commons.jwt.JWTAuthenticationFilter;

@EnableWebSecurity
@Import(JWTConfiguration.class)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private JWTAuthenticationFilter jwtAuthenticationFilter;

	public SecurityConfiguration(JWTAuthenticationFilter jwtAuthenticationFilter) {
		this.jwtAuthenticationFilter = jwtAuthenticationFilter;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().csrf().disable()
				.addFilterBefore(jwtAuthenticationFilter, BasicAuthenticationFilter.class)
				.authorizeRequests()
				.antMatchers("/health", "/login").anonymous()
				.anyRequest().hasRole("API");
	}
}
