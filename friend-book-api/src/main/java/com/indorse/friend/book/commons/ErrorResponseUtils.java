package com.indorse.friend.book.commons;

import java.util.Map;

import javax.validation.ConstraintViolationException;

public class ErrorResponseUtils {

	public static ErrorResponse fromException(ConstraintViolationException e) {
		Map<String, String> errors = ValidationUtils.toMap(e.getConstraintViolations());
		return new ErrorResponse(errors);
	}

	public static ErrorResponse fromException(Exception e) {
		return new ErrorResponse(e.getMessage());
	}
}