package com.indorse.friend.book.entities;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "user_friend")
public class UserFriend extends AbstractEntity {

	private static final long serialVersionUID = -1460104633801435193L;
	@Column(name = "user_id")
	private UUID userId;
	@Column(name = "friend_id")
	private UUID friendId;
	@Column(name = "is_active")
	private Boolean isActive;
	@Column(name = "is_blocked")
	private Boolean isBlocked;

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public UUID getFriendId() {
		return friendId;
	}

	public void setFriendId(UUID friendId) {
		this.friendId = friendId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
}
