package com.indorse.friend.book.user.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indorse.friend.book.commons.AbstractEndpoint;
import com.indorse.friend.book.commons.RestAPIException;
import com.indorse.friend.book.entities.UserProfile;
import com.indorse.friend.book.mappings.UserMappingService;
import com.indorse.friend.book.models.UserProfileDTO;
import com.indorse.friend.book.user.service.UserCreateRequest;
import com.indorse.friend.book.user.service.UserCreateService;

@RestController
public class UserCreateController extends AbstractEndpoint {

	public final UserMappingService userMappingService;
	private final UserCreateService userCreateService;
	
	public UserCreateController(UserMappingService userMappingService,
			UserCreateService userCreateService) {
		this.userMappingService = userMappingService;
		this.userCreateService = userCreateService;
	}
	
	@RequestMapping(
            value = "/user",
            method = RequestMethod.POST)
    public UserProfileDTO createUserProfile(@RequestBody UserCreateRequest request) throws Exception {
		if(validateRequest(request)) {
	    	UserProfile user = userCreateService.execute(request);
	    	if(user == null) {
	    		throw new RestAPIException("Unable to create user profile");
	    	}
	    	return userMappingService.map(user);
		} 
		return null;
    }
	
	private boolean validateRequest(UserCreateRequest request) throws Exception {
		if(StringUtils.isBlank(request.getEmail())) {
			throw new RestAPIException("Bad request, email is null");
		} else if(StringUtils.isBlank(request.getPassword())) {
			throw new RestAPIException("Bad request, password is null");
		}
		return true;
	}
}
