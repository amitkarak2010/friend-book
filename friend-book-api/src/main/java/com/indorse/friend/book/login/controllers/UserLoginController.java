package com.indorse.friend.book.login.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indorse.friend.book.commons.AbstractEndpoint;
import com.indorse.friend.book.commons.RestAPIException;
import com.indorse.friend.book.login.service.UserLoginRequest;
import com.indorse.friend.book.login.service.UserLoginService;
import com.indorse.friend.book.models.UserProfileDTO;

@RestController
public class UserLoginController extends AbstractEndpoint {
	private final UserLoginService userLoginService;
	
	public UserLoginController(UserLoginService userLoginService) {
		this.userLoginService = userLoginService;
	}
	
	@RequestMapping(
            value = "/login",
            method = RequestMethod.POST)
    public UserProfileDTO login(@RequestBody UserLoginRequest request) throws Exception {
		if(validateRequest(request)) {
	    	return userLoginService.execute(request);
		} 
		return null;
    }
	
	private boolean validateRequest(UserLoginRequest request) throws Exception {
		if(StringUtils.isBlank(request.getEmail())) {
			throw new RestAPIException("Bad request, email is null");
		} else if(StringUtils.isBlank(request.getPassword())) {
			throw new RestAPIException("Bad request, password is null");
		}
		return true;
	}
}
