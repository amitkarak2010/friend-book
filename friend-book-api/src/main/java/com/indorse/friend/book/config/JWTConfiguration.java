package com.indorse.friend.book.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.indorse.friend.book.commons.jwt")
public class JWTConfiguration {

}