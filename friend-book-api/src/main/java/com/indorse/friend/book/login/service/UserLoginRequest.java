package com.indorse.friend.book.login.service;

import com.indorse.friend.book.models.UserProfileDTO;

public class UserLoginRequest extends UserProfileDTO {

	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
