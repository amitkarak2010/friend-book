package com.indorse.friend.book.mappings;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.indorse.friend.book.entities.UserFriend;
import com.indorse.friend.book.entities.UserProfile;
import com.indorse.friend.book.friend.service.FriendRequest;
import com.indorse.friend.book.models.UserProfileDTO;
import com.indorse.friend.book.user.service.UserCreateRequest;

@Service
public class UserMappingService {

	private final ModelMapper modelMapper;
	
	public UserMappingService() {
		modelMapper = new ModelMapper();
	}
	
	public UserProfileDTO map(UserProfile request) {
		return this.modelMapper.map(request, UserProfileDTO.class);
	}
	
	public UserProfile map(UserCreateRequest request) {
		return this.modelMapper.map(request, UserProfile.class);
	}
	
	public UserFriend map(FriendRequest request) {
		UserFriend friend = new UserFriend();
		friend.setUserId(request.getUserId());
		friend.setFriendId(request.getFriendId());
		friend.setIsActive(true);
		friend.setIsBlocked(false);
		return friend;
	}
}
