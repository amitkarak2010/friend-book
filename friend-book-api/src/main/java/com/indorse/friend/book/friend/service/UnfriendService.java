package com.indorse.friend.book.friend.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.indorse.friend.book.commons.NotFoundException;
import com.indorse.friend.book.entities.UserFriend;
import com.indorse.friend.book.repositories.UserFriendRepository;
import com.querydsl.core.types.Predicate;

import static com.indorse.friend.book.repositories.UserFriendPredicates.withUserAndFriendId;

@Service
public class UnfriendService {

	private UserFriendRepository userFriendRepository;
	
	public UnfriendService(UserFriendRepository userFriendRepository) {
		this.userFriendRepository = userFriendRepository;
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public boolean execute(FriendRequest request) throws Exception {
		Predicate predicate = withUserAndFriendId(request.getUserId(), request.getFriendId());
		UserFriend userFriend = userFriendRepository.findOne(predicate);
		if(userFriend == null) {
			throw new NotFoundException("No friend with friend id: " + request.getFriendId() 
				+ " exists for user id : " + request.getUserId());
		}
		userFriendRepository.delete(userFriend);
		return true;
	}
}
