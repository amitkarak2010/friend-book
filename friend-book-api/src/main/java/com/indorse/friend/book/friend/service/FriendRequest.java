package com.indorse.friend.book.friend.service;

import java.util.UUID;

public class FriendRequest {

	private UUID userId;
	private UUID friendId;

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public UUID getFriendId() {
		return friendId;
	}

	public void setFriendId(UUID friendId) {
		this.friendId = friendId;
	}

}
