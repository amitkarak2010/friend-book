package com.indorse.friend.book.user.controllers;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indorse.friend.book.commons.AbstractEndpoint;
import com.indorse.friend.book.entities.UserProfile;
import com.indorse.friend.book.user.service.UserListRequest;
import com.indorse.friend.book.user.service.UserListService;

@RestController
public class UserListController extends AbstractEndpoint {

	private final UserListService userListService;
	
	public UserListController(UserListService userListService) {
		this.userListService = userListService;
	}
	
	@RequestMapping(
            value = "/user/{userId}/list",
            method = RequestMethod.GET)
    public Page<UserProfile> listUsers(@PathVariable UUID userId,
    		@SortDefault.SortDefaults({
                @SortDefault(sort = "firstName", direction = Direction.DESC)
            })
            Pageable pageable) throws Exception {
		
		return userListService.execute(new UserListRequest().withPageable(pageable).withUserId(userId));
	}
}
