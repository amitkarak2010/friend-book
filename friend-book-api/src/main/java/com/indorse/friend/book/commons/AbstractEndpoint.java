package com.indorse.friend.book.commons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

public abstract class AbstractEndpoint {

	protected static final Logger LOG = LoggerFactory.getLogger(AbstractEndpoint.class);

    @ExceptionHandler({RestAPIException.class})
    protected ResponseEntity<ErrorResponse> handleRestApiException(RestAPIException ex) {
        LOG.info("Caught RestApiException:" + ex.getMessage());
        ErrorResponse errorResponse = ErrorResponseUtils.fromException(ex);
        return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
	@ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<ErrorResponse> handleBadRequestException(
    		NotFoundException ex) {
        LOG.error("Caught BadRequestException", ex);
        ErrorResponse errorResponse = ErrorResponseUtils.fromException(ex);
        return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler({ Exception.class })
	protected ResponseEntity<?> handleException(Exception ex) {
		LOG.error("Error processing request", ex);

		return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
