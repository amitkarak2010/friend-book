package com.indorse.friend.book.friend.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.indorse.friend.book.commons.RestAPIException;
import com.indorse.friend.book.entities.UserFriend;
import com.indorse.friend.book.mappings.UserMappingService;
import com.indorse.friend.book.repositories.UserFriendRepository;
import com.querydsl.core.types.Predicate;

import static com.indorse.friend.book.repositories.UserFriendPredicates.withUserAndFriendId;

@Service
public class FriendService {

	private UserFriendRepository userFriendRepository;
	private UserMappingService userMappingService;
	
	public FriendService(UserFriendRepository userFriendRepository,
			UserMappingService userMappingService) {
		this.userFriendRepository = userFriendRepository;
		this.userMappingService = userMappingService;
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public boolean execute(FriendRequest request) throws Exception {
		Predicate predicate = withUserAndFriendId(request.getUserId(), request.getFriendId());
		UserFriend userFriend = userFriendRepository.findOne(predicate);
		if(userFriend != null) {
			throw new RestAPIException("Friend already exists with friend id: " + request.getFriendId()
					+ " for user id : " + request.getUserId());
		}
		userFriend = userMappingService.map(request);
		userFriendRepository.save(userFriend);
		return true;
	}
}
