package com.indorse.friend.book.friend.controllers;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indorse.friend.book.commons.RestAPIException;
import com.indorse.friend.book.friend.service.FriendRequest;
import com.indorse.friend.book.friend.service.UnfriendService;

@RestController
public class UnfriendController {
	private final UnfriendService unfriendService;
	
	public UnfriendController(UnfriendService unfriendService) {
		this.unfriendService = unfriendService;
	}
	
	@RequestMapping(
            value = "/user/remove/friend",
            method = RequestMethod.POST)
    public Boolean removeFriend(@RequestBody FriendRequest request) throws Exception {
		if(validateRequest(request)) {
	    	return unfriendService.execute(request);
		} 
		return null;
    }
	
	private boolean validateRequest(FriendRequest request) throws Exception {
		if(request.getUserId() == null) {
			throw new RestAPIException("Bad request, user id is null");
		} else if(request.getFriendId() == null) {
			throw new RestAPIException("Bad request, friend id is null");
		}
		return true;
	}
}
