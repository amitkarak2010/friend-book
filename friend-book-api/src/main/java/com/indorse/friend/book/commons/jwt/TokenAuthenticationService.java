package com.indorse.friend.book.commons.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.indorse.friend.book.config.AuthProperties;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Help generating the token based on HS512 algo. Main class to generate token for a role
 * Will be used to authenticate users for the API accessibility.
 * 
 * I am writing it based on JWT, though we can use some OAUTH 2.0 based servers which will
 * take lot of time to implement but thats the right approach if the api is public, if its
 * private api for internal application use we can live with jwt behind firewall.
 * @author amitkumar
 *
 */
@Service
public class TokenAuthenticationService {

    private static final Logger LOG = LoggerFactory.getLogger(TokenAuthenticationService.class);

    private final AuthProperties authProperties;

    public TokenAuthenticationService(AuthProperties authProperties) {
        this.authProperties = authProperties;
    }

    public String generateToken(String username, String[] roles, Date expiration) {

        String jwt = Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(username)
                .setExpiration(expiration)
                .claim("roles", roles)
                .signWith(SignatureAlgorithm.HS512, authProperties.getSecret()).compact();

        return jwt;
    }

    public String generateToken(String username, String[] roles) {

        return generateToken(username, roles, null);
    }

    public Authentication getAuthentication(HttpServletRequest request) {
        try {
            String token = request.getHeader(authProperties.getHeader());

            if (token != null) {
                String jwt = token.replace(authProperties.getTokenPrefix(), "");

                Claims claims = Jwts.parser()
                        .setSigningKey(authProperties.getSecret())
                        .parseClaimsJws(jwt)
                        .getBody();

                String username = claims.getSubject();
                @SuppressWarnings("unchecked")
                List<String> roles = (List<String>) claims.get("roles");

                return buildAuthentication(username, roles);

            }
        } catch (JwtException e) {
            LOG.info(e.getMessage());
        } catch (Exception e) {
            LOG.info("Error validating JWT", e);
        }
        return null;
    }

    private Authentication buildAuthentication(String username, List<String> roles) {
        if (username != null) {
            UserDetails userDetails = User.withUsername(username)
                    .password(randomPassword())
                    .roles(toRoleStrings(roles))
                    .build();

            return new UsernamePasswordAuthenticationToken(
                    userDetails.getUsername(),
                    userDetails.getPassword(),
                    userDetails.getAuthorities());
        } else {
            LOG.info("Invalid JWT token, username is null");
            return null;
        }
    }

    private String[] toRoleStrings(List<String> roles) {
        if (roles != null) {
            return roles.stream().toArray(String[]::new);
        }

        return new String[0];
    }

    private String randomPassword() {
        return UUID.randomUUID().toString();
    }

}
