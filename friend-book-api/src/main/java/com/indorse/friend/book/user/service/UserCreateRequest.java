package com.indorse.friend.book.user.service;

import com.indorse.friend.book.models.UserProfileDTO;

public class UserCreateRequest extends UserProfileDTO {

	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
