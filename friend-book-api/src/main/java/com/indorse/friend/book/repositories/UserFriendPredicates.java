package com.indorse.friend.book.repositories;

import java.util.UUID;


import com.indorse.friend.book.entities.QUserFriend;
import com.querydsl.core.types.dsl.BooleanExpression;

public class UserFriendPredicates {

	public static BooleanExpression withUserAndFriendId(UUID userId, UUID friendId) {
		QUserFriend userFriend = QUserFriend.userFriend;
		if(userId == null || friendId == null) {
			return null;
		}
		return userFriend.userId.eq(userId).and(userFriend.friendId.eq(friendId));
	}
}
