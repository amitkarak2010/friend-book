package com.indorse.friend.book.user.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.indorse.friend.book.entities.UserProfile;
import com.indorse.friend.book.mappings.UserMappingService;
import com.indorse.friend.book.repositories.UserProfileRepository;
import com.indorse.friend.book.utils.PasswordService;

@Service
public class UserCreateService {

	private final UserProfileRepository userProfileRepository;
	private final UserMappingService userMappingService;
	private final PasswordService passwordService;
	
	public UserCreateService(UserProfileRepository userProfileRepository,
			UserMappingService userMappingService,
			PasswordService passwordService) {
		this.userProfileRepository = userProfileRepository;
		this.userMappingService = userMappingService;
		this.passwordService = passwordService;
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public UserProfile execute(UserCreateRequest request) throws Exception {
		UserProfile user = userMappingService.map(request);
		String password = passwordService.encodePassword(request.getPassword());
		user.setPassword(password);
		user = userProfileRepository.save(user);
		return user;
	}
}
