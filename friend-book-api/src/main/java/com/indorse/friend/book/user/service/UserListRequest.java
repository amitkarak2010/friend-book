package com.indorse.friend.book.user.service;

import java.util.UUID;

import org.springframework.data.domain.Pageable;

public class UserListRequest {

	private UUID userId;
	private Pageable pageable;

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public UserListRequest withUserId(UUID userId) {
		this.userId = userId;
		return this;
	}
	
	public Pageable getPageable() {
		return pageable;
	}

	public void setPageable(Pageable pageable) {
		this.pageable = pageable;
	}

	public UserListRequest withPageable(Pageable pageable) {
		this.pageable = pageable;
		return this;
	}
}
