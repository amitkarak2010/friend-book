package com.indorse.friend.book.commons;

public class RestAPIException extends Exception {

	private static final long serialVersionUID = -7635095387793482726L;

	public RestAPIException() {
		super();
	}

	public RestAPIException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RestAPIException(String message, Throwable cause) {
		super(message, cause);
	}

	public RestAPIException(String message) {
		super(message);
	}

	public RestAPIException(Throwable cause) {
		super(cause);
	}

}
