package com.indorse.friend.book.repositories;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import com.indorse.friend.book.entities.QUserProfile;
import com.querydsl.core.types.dsl.BooleanExpression;

public class UserPredicates {


	public static BooleanExpression withEmail(String email) {
		QUserProfile userProfile = QUserProfile.userProfile;
		if(StringUtils.isBlank(email)) {
			return null;
		}
		return userProfile.email.eq(email);
	}
	
	public static BooleanExpression withOtherUser(UUID userId) {
		QUserProfile userProfile = QUserProfile.userProfile;
		if(userId == null) {
			return null;
		}
		return userProfile.id.ne(userId);
	}
}
