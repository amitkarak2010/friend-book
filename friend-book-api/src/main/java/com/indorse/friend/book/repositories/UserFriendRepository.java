package com.indorse.friend.book.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.indorse.friend.book.entities.UserFriend;

public interface UserFriendRepository 
		extends JpaRepository<UserFriend, UUID>, QueryDslPredicateExecutor<UserFriend> {

}
