package com.indorse.friend.book.entities;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUserFriend is a Querydsl query type for UserFriend
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserFriend extends EntityPathBase<UserFriend> {

    private static final long serialVersionUID = -1079212038L;

    public static final QUserFriend userFriend = new QUserFriend("userFriend");

    public final QAbstractEntity _super = new QAbstractEntity(this);

    //inherited
    public final DateTimePath<java.util.Date> created = _super.created;

    public final ComparablePath<java.util.UUID> friendId = createComparable("friendId", java.util.UUID.class);

    //inherited
    public final ComparablePath<java.util.UUID> id = _super.id;

    public final BooleanPath isActive = createBoolean("isActive");

    public final BooleanPath isBlocked = createBoolean("isBlocked");

    //inherited
    public final DateTimePath<java.util.Date> lastModified = _super.lastModified;

    public final ComparablePath<java.util.UUID> userId = createComparable("userId", java.util.UUID.class);

    public QUserFriend(String variable) {
        super(UserFriend.class, forVariable(variable));
    }

    public QUserFriend(Path<? extends UserFriend> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUserFriend(PathMetadata metadata) {
        super(UserFriend.class, metadata);
    }

}

