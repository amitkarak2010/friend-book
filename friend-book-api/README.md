# friend-book
Microservice for managing user profile and friends

## Requirements

* Java 8
* Postgres 9.x+
* Gradle 4.5.x or above
* Eclipse or other IDE having gradle

## Setup

This repo contains an Spring Boot framework application. To setup the dev environment do the following:

* Clone the repo
* Import the gradle project inside eclipse.

## Setup using command prompt

* To build the project locally go inside friend-book-api folder and run below command

   gradle clean build

* To creat / update the database make sure to follow below steps from postgres command prompt
   * create database <database_name>
   * grant TEMPORARY, CONNECT ON DATABASE <database_name> TO PUBLIC;
   * grant ALL ON DATABASE <database_name> TO <username>;

   * Once done configure following environment variable
   	* export DB_URL=jdbc:postgresql://<hostname>:<port>/<database_name>
   	* export DB_USERNAME=<username>
   	* export DB_PASSWORD=<password>

   * Once the database is done, run the below command to execute liquibase scripts to create database
   	* gradle update

* Once the database update and creation is done, run the following command to start the service from friend-book-api folder
   * Make sure you configured the datasource inside the application.yml file.
   * Run "java -jar build/libs/indorse-friend-book-api-0.1.0.jar" to start the service
   
* To generate the JWT token for API's with API role run the below command from the friend-book-api folder.
     java -jar ./build/libs/indorse-friend-book-api-0.1.0.jar --generate-jwt --roles API --username Amit

* To access all the REST API's please go to the folder data/request
	APIs in the microservices
	* Create user which registers a user. I have protected it with token might be we dont wanna protect registration with token but for time being I have protected it with jwt.
	* List user which will list paginated users except the user which is passed in the reuqest.
	* Login API to login with user name and password
	* Helath API to tell whether microservices are up or not
	* Add friend api
	* Remove friend api.
